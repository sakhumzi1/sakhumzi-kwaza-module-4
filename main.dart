import 'package:flutter/material.dart';
import 'pages/splash_screen.dart';

import 'package:hexcolor/hexcolor.dart';

void main() {
  runApp(SakhumziLoginApp());
}

class SakhumziLoginApp extends StatelessWidget {
  //Color _primaryColor = HexColor('#DC54FE');
  //Color _accentColor = HexColor('#8A02AE');

  // Design color

  final Color _primaryColor = HexColor('#FFC867');
  final Color _accentColor = HexColor('#FF3CBD');

  SakhumziLoginApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Sakhumzi Login',
      theme: ThemeData(
        primaryColor: _primaryColor,
        scaffoldBackgroundColor: Colors.grey.shade100,
        colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.grey)
            .copyWith(secondary: _accentColor),
      ),
      home: SplashScreen(title: 'Sakhumzi Login'),
    );
  }
}
